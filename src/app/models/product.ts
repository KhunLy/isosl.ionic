export interface ProductRequest {
  code: string;
  product: Product;
  status: number;
  status_verbose: string;
}

interface Product {
  _id: string;
  _keywords: string[];
  added_countries_tags: any[];
  additives_debug_tags: any[];
  additives_n: number;
  additives_old_n: number;
  additives_old_tags: string[];
  additives_original_tags: string[];
  additives_prev_original_tags: string[];
  additives_tags: string[];
  additives_tags_n?: any;
  allergens: string;
  allergens_from_ingredients: string;
  allergens_from_user: string;
  allergens_hierarchy: any[];
  allergens_lc: string;
  allergens_tags: any[];
  amino_acids_prev_tags: any[];
  amino_acids_tags: any[];
  brands: string;
  brands_tags: string[];
  categories: string;
  categories_hierarchy: string[];
  categories_lc: string;
  categories_old: string;
  categories_properties: Categoriesproperties;
  categories_properties_tags: string[];
  categories_tags: string[];
  category_properties: Categoryproperties;
  checkers: any[];
  checkers_tags: any[];
  ciqual_food_name_tags: string[];
  cities_tags: any[];
  code: string;
  codes_tags: string[];
  compared_to_category: string;
  complete: number;
  completeness: number;
  correctors: any[];
  correctors_tags: string[];
  countries: string;
  countries_beforescanbot: string;
  countries_hierarchy: string[];
  countries_lc: string;
  countries_tags: string[];
  created_t: number;
  creator: string;
  data_quality_bugs_tags: any[];
  data_quality_errors_tags: any[];
  data_quality_info_tags: string[];
  data_quality_tags: string[];
  data_quality_warnings_tags: any[];
  data_sources: string;
  data_sources_tags: string[];
  debug_param_sorted_langs: string[];
  debug_tags: string[];
  ecoscore_data: Ecoscoredata;
  ecoscore_extended_data: Ecoscoreextendeddata;
  ecoscore_extended_data_version: string;
  ecoscore_grade: string;
  ecoscore_tags: string[];
  editors: string[];
  editors_tags: string[];
  emb_codes: string;
  emb_codes_20141016: string;
  emb_codes_orig: string;
  emb_codes_tags: any[];
  entry_dates_tags: string[];
  environment_impact_level: string;
  environment_impact_level_tags: any[];
  expiration_date: string;
  food_groups: string;
  food_groups_tags: string[];
  'fruits-vegetables-nuts_100g_estimate': number;
  generic_name: string;
  generic_name_ar: string;
  generic_name_az: string;
  generic_name_bg: string;
  generic_name_de: string;
  generic_name_en: string;
  generic_name_es: string;
  generic_name_fr: string;
  generic_name_it: string;
  generic_name_ka: string;
  generic_name_pl: string;
  generic_name_pt: string;
  generic_name_ro: string;
  generic_name_ru: string;
  generic_name_sq: string;
  generic_name_xx: string;
  generic_name_xx_debug_tags: any[];
  id: string;
  image_front_small_url: string;
  image_front_thumb_url: string;
  image_front_url: string;
  image_ingredients_small_url: string;
  image_ingredients_thumb_url: string;
  image_ingredients_url: string;
  image_nutrition_small_url: string;
  image_nutrition_thumb_url: string;
  image_nutrition_url: string;
  image_packaging_small_url: string;
  image_packaging_thumb_url: string;
  image_packaging_url: string;
  image_small_url: string;
  image_thumb_url: string;
  image_url: string;
  images: Images;
  informers: string[];
  informers_tags: string[];
  ingredients: Ingredient[];
  ingredients_analysis: Adjustments;
  ingredients_analysis_tags: string[];
  ingredients_debug: (null | string)[];
  ingredients_from_or_that_may_be_from_palm_oil_n: number;
  ingredients_from_palm_oil_n: number;
  ingredients_from_palm_oil_tags: any[];
  ingredients_hierarchy: string[];
  ingredients_ids_debug: string[];
  ingredients_n: number;
  ingredients_n_tags: string[];
  ingredients_original_tags: string[];
  ingredients_percent_analysis: number;
  ingredients_tags: string[];
  ingredients_text: string;
  ingredients_text_ar: string;
  ingredients_text_az: string;
  ingredients_text_bg: string;
  ingredients_text_de: string;
  ingredients_text_debug: string;
  ingredients_text_en: string;
  ingredients_text_es: string;
  ingredients_text_fr: string;
  ingredients_text_it: string;
  ingredients_text_ka: string;
  ingredients_text_pl: string;
  ingredients_text_pt: string;
  ingredients_text_ro: string;
  ingredients_text_ru: string;
  ingredients_text_sq: string;
  ingredients_text_with_allergens: string;
  ingredients_text_with_allergens_ar: string;
  ingredients_text_with_allergens_az: string;
  ingredients_text_with_allergens_bg: string;
  ingredients_text_with_allergens_de: string;
  ingredients_text_with_allergens_en: string;
  ingredients_text_with_allergens_es: string;
  ingredients_text_with_allergens_fr: string;
  ingredients_text_with_allergens_it: string;
  ingredients_text_with_allergens_ka: string;
  ingredients_text_with_allergens_pl: string;
  ingredients_text_with_allergens_pt: string;
  ingredients_text_with_allergens_ro: string;
  ingredients_text_with_allergens_ru: string;
  ingredients_text_with_allergens_sq: string;
  ingredients_text_xx: string;
  ingredients_text_xx_debug_tags: any[];
  ingredients_that_may_be_from_palm_oil_n: number;
  ingredients_that_may_be_from_palm_oil_tags: any[];
  ingredients_with_specified_percent_n: number;
  ingredients_with_specified_percent_sum: number;
  ingredients_with_unspecified_percent_n: number;
  ingredients_with_unspecified_percent_sum: number;
  interface_version_modified: string;
  known_ingredients_n: number;
  labels: string;
  labels_hierarchy: string[];
  labels_lc: string;
  labels_old: string;
  labels_tags: string[];
  lang: string;
  languages: Languages;
  languages_codes: Languagescodes;
  languages_hierarchy: string[];
  languages_tags: string[];
  last_edit_dates_tags: string[];
  last_editor: string;
  last_image_dates_tags: string[];
  last_image_t: number;
  last_modified_by: string;
  last_modified_t: number;
  lc: string;
  link: string;
  main_countries_tags: any[];
  manufacturing_places: string;
  manufacturing_places_tags: any[];
  max_imgid: string;
  minerals_prev_tags: any[];
  minerals_tags: any[];
  misc_tags: string[];
  new_additives_n: number;
  no_nutrition_data: string;
  nova_group: number;
  nova_group_debug: string;
  nova_group_tags: string[];
  nova_groups: string;
  nova_groups_tags: string[];
  nucleotides_prev_tags: any[];
  nucleotides_tags: any[];
  nutrient_levels: Nutrientlevels;
  nutrient_levels_tags: string[];
  nutriments: Nutriments;
  nutriscore_data: Nutriscoredata;
  nutriscore_grade: string;
  nutriscore_score: number;
  nutriscore_score_opposite: number;
  nutrition_data: string;
  nutrition_data_per: string;
  nutrition_data_prepared: string;
  nutrition_data_prepared_per: string;
  nutrition_grade_fr: string;
  nutrition_grades: string;
  nutrition_grades_tags: string[];
  nutrition_score_beverage: number;
  nutrition_score_warning_fruits_vegetables_nuts_estimate_from_ingredients: number;
  nutrition_score_warning_fruits_vegetables_nuts_estimate_from_ingredients_value: number;
  obsolete: string;
  obsolete_since_date: string;
  origin: string;
  origin_ar: string;
  origin_az: string;
  origin_bg: string;
  origin_de: string;
  origin_en: string;
  origin_es: string;
  origin_fr: string;
  origin_it: string;
  origin_ka: string;
  origin_pl: string;
  origin_pt: string;
  origin_ro: string;
  origin_ru: string;
  origin_sq: string;
  origins: string;
  origins_hierarchy: any[];
  origins_lc: string;
  origins_old: string;
  origins_tags: any[];
  other_nutritional_substances_prev_tags: any[];
  other_nutritional_substances_tags: any[];
  packaging: string;
  packaging_hierarchy: string[];
  packaging_lc: string;
  packaging_old: string;
  packaging_old_before_taxonomization: string;
  packaging_tags: string[];
  packaging_text: string;
  packaging_text_ar: string;
  packaging_text_az: string;
  packaging_text_bg: string;
  packaging_text_de: string;
  packaging_text_en: string;
  packaging_text_es: string;
  packaging_text_fr: string;
  packaging_text_it: string;
  packaging_text_ka: string;
  packaging_text_pl: string;
  packaging_text_pt: string;
  packaging_text_ro: string;
  packaging_text_ru: string;
  packaging_text_sq: string;
  packagings: Packaging[];
  photographers: string[];
  photographers_tags: string[];
  pnns_groups_1: string;
  pnns_groups_1_tags: string[];
  pnns_groups_2: string;
  pnns_groups_2_tags: string[];
  popularity_key: number;
  popularity_tags: string[];
  product_name: string;
  product_name_ar: string;
  product_name_az: string;
  product_name_bg: string;
  product_name_de: string;
  product_name_en: string;
  product_name_es: string;
  product_name_fr: string;
  product_name_it: string;
  product_name_ka: string;
  product_name_pl: string;
  product_name_pt: string;
  product_name_ro: string;
  product_name_ru: string;
  product_name_sq: string;
  product_name_xx: string;
  product_name_xx_debug_tags: any[];
  product_quantity: string;
  purchase_places: string;
  purchase_places_tags: string[];
  quantity: string;
  removed_countries_tags: any[];
  rev: number;
  scans_n: number;
  selected_images: Selectedimages;
  serving_quantity: string;
  serving_size: string;
  sortkey: number;
  sources: Source[];
  states: string;
  states_hierarchy: string[];
  states_tags: string[];
  stores: string;
  stores_tags: any[];
  teams: string;
  teams_tags: string[];
  traces: string;
  traces_from_ingredients: string;
  traces_from_user: string;
  traces_hierarchy: any[];
  traces_lc: string;
  traces_tags: any[];
  unique_scans_n: number;
  unknown_ingredients_n: number;
  unknown_nutrients_tags: any[];
  update_key: string;
  vitamins_prev_tags: any[];
  vitamins_tags: any[];
}

interface Source {
  fields: string[];
  id: string;
  images: string[];
  import_t: number;
  url: string;
  manufacturer?: string;
  name?: string;
  source_licence?: string;
  source_licence_url?: string;
}

interface Selectedimages {
  front: Front;
  ingredients: Ingredients2;
  nutrition: Nutrition;
  packaging: Packaging2;
}

interface Packaging2 {
  display: Display4;
  small: Display4;
  thumb: Display4;
}

interface Display4 {
  az: string;
  bg: string;
  en: string;
  fr: string;
  ka: string;
  pl: string;
  ro: string;
  sq: string;
}

interface Nutrition {
  display: Display3;
  small: Display3;
  thumb: Display3;
}

interface Display3 {
  az: string;
  bg: string;
  de: string;
  en: string;
  es: string;
  fr: string;
  ka: string;
  pl: string;
  ro: string;
  sq: string;
}

interface Ingredients2 {
  display: Display2;
  small: Display2;
  thumb: Display2;
}

interface Display2 {
  ar: string;
  az: string;
  bg: string;
  de: string;
  en: string;
  es: string;
  fr: string;
  it: string;
  ka: string;
  pl: string;
  ro: string;
  sq: string;
}

interface Front {
  display: Display;
  small: Display;
  thumb: Display;
}

interface Display {
  ar: string;
  az: string;
  de: string;
  en: string;
  es: string;
  fr: string;
  it: string;
  ka: string;
  pl: string;
  pt: string;
  ro: string;
  sq: string;
}

interface Packaging {
  material: string;
  number: string;
  recycling: string;
  shape: string;
}

interface Nutriscoredata {
  energy: number;
  energy_points: number;
  energy_value: number;
  fiber: number;
  fiber_points: number;
  fiber_value: number;
  fruits_vegetables_nuts_colza_walnut_olive_oils: number;
  fruits_vegetables_nuts_colza_walnut_olive_oils_points: number;
  fruits_vegetables_nuts_colza_walnut_olive_oils_value: number;
  grade: string;
  is_beverage: number;
  is_cheese: number;
  is_fat: number;
  is_water: number;
  negative_points: number;
  positive_points: number;
  proteins: number;
  proteins_points: number;
  proteins_value: number;
  saturated_fat: number;
  saturated_fat_points: number;
  saturated_fat_ratio: number;
  saturated_fat_ratio_points: number;
  saturated_fat_ratio_value: number;
  saturated_fat_value: number;
  score: number;
  sodium: number;
  sodium_points: number;
  sodium_value: number;
  sugars: number;
  sugars_points: number;
  sugars_value: number;
}

interface Nutriments {
  carbohydrates: number;
  carbohydrates_100g: number;
  carbohydrates_serving: number;
  carbohydrates_unit: string;
  carbohydrates_value: number;
  energy: number;
  'energy-kcal': number;
  'energy-kcal_100g': number;
  'energy-kcal_serving': number;
  'energy-kcal_unit': string;
  'energy-kcal_value': number;
  'energy-kj': number;
  'energy-kj_100g': number;
  'energy-kj_serving': number;
  'energy-kj_unit': string;
  'energy-kj_value': number;
  energy_100g: number;
  energy_serving: number;
  energy_unit: string;
  energy_value: number;
  fat: number;
  fat_100g: number;
  fat_serving: number;
  fat_unit: string;
  fat_value: number;
  fiber: number;
  fiber_100g: number;
  fiber_serving: number;
  fiber_unit: string;
  fiber_value: number;
  'fruits-vegetables-nuts-estimate-from-ingredients_100g': number;
  'fruits-vegetables-nuts-estimate-from-ingredients_serving': number;
  'nova-group': number;
  'nova-group_100g': number;
  'nova-group_serving': number;
  'nutrition-score-fr': number;
  'nutrition-score-fr_100g': number;
  proteins: number;
  proteins_100g: number;
  proteins_serving: number;
  proteins_unit: string;
  proteins_value: number;
  salt: number;
  salt_100g: number;
  salt_serving: number;
  salt_unit: string;
  salt_value: number;
  'saturated-fat': number;
  'saturated-fat_100g': number;
  'saturated-fat_serving': number;
  'saturated-fat_unit': string;
  'saturated-fat_value': number;
  sodium: number;
  sodium_100g: number;
  sodium_serving: number;
  sodium_unit: string;
  sodium_value: number;
  sugars: number;
  sugars_100g: number;
  sugars_serving: number;
  sugars_unit: string;
  sugars_value: number;
}

interface Nutrientlevels {
  fat: string;
  salt: string;
  'saturated-fat': string;
  sugars: string;
}

interface Languagescodes {
  ar: number;
  az: number;
  bg: number;
  de: number;
  en: number;
  es: number;
  fr: number;
  it: number;
  ka: number;
  pl: number;
  pt: number;
  ro: number;
  ru: number;
  sq: number;
}

interface Languages {
  'en:albanian': number;
  'en:arabic': number;
  'en:azerbaijani': number;
  'en:bulgarian': number;
  'en:english': number;
  'en:french': number;
  'en:georgian': number;
  'en:german': number;
  'en:italian': number;
  'en:polish': number;
  'en:portuguese': number;
  'en:romanian': number;
  'en:russian': number;
  'en:spanish': number;
}

interface Ingredient {
  id: string;
  percent_estimate: number;
  percent_max: number;
  percent_min: number;
  rank?: number;
  text: string;
  vegan?: string;
  vegetarian?: string;
  has_sub_ingredients?: string;
  labels?: string;
}

interface Images {
  '1': _1;
  '2': _1;
  '3': _1;
  '4': _1;
  '5': _1;
  '6': _1;
  '7': _1;
  '8': _1;
  '9': _1;
  '10': _1;
  '11': _1;
  '12': _1;
  '13': _1;
  '14': _1;
  '15': _1;
  '16': _1;
  '17': _1;
  '18': _1;
  '19': _1;
  '20': _1;
  '21': _1;
  '23': _1;
  '24': _1;
  '25': _1;
  '33': _1;
  '35': _35;
  '36': _35;
  '37': _35;
  '38': _35;
  '43': _35;
  '44': _35;
  '45': _35;
  '47': _35;
  '48': _35;
  '49': _35;
  '51': _35;
  '53': _35;
  '56': _35;
  '57': _35;
  '58': _35;
  '59': _35;
  '60': _35;
  '65': _35;
  '68': _35;
  '69': _35;
  '71': _35;
  '73': _1;
  '74': _1;
  '75': _1;
  '76': _1;
  '77': _35;
  '79': _35;
  '81': _35;
  '82': _35;
  '83': _35;
  '85': _35;
  '87': _35;
  '89': _35;
  '90': _35;
  '91': _35;
  '110': _35;
  '112': _35;
  '114': _35;
  '115': _35;
  '116': _35;
  '117': _35;
  '119': _35;
  '120': _35;
  '121': _35;
  '122': _35;
  '123': _35;
  '125': _35;
  '129': _1;
  '131': _1;
  '132': _1;
  '133': _1;
  '134': _1;
  '142': _1;
  '146': _1;
  '148': _1;
  '149': _1;
  '150': _35;
  '152': _35;
  '153': _1;
  '158': _1;
  '159': _1;
  '161': _1;
  '162': _1;
  '165': _1;
  '166': _1;
  '168': _1;
  '169': _1;
  '170': _1;
  '171': _1;
  '173': _1;
  '175': _1;
  '176': _1;
  '177': _1;
  '180': _1;
  '182': _1;
  '183': _1;
  '184': _1;
  '189': _1;
  '190': _1;
  '191': _1;
  '192': _1;
  '193': _1;
  '194': _35;
  '195': _35;
  '196': _35;
  '197': _1;
  '198': _1;
  '199': _1;
  '201': _1;
  '202': _1;
  '203': _1;
  '204': _1;
  '205': _1;
  '206': _1;
  '208': _1;
  '209': _1;
  '210': _1;
  '211': _1;
  '212': _1;
  '213': _1;
  front_ar: Frontar;
  front_az: Frontaz;
  front_de: Frontaz;
  front_en: Frontaz;
  front_es: Frontaz;
  front_fr: Frontaz;
  front_it: Frontaz;
  front_ka: Frontaz;
  front_other: Frontother;
  front_pl: Frontar;
  front_pt: Frontar;
  front_ro: Frontar;
  front_sq: Frontaz;
  ingredients: Ingredients;
  ingredients_ar: Frontaz;
  ingredients_az: Frontaz;
  ingredients_bg: Frontaz;
  ingredients_de: Ingredientsde;
  ingredients_en: Frontaz;
  ingredients_es: Frontaz;
  ingredients_fr: Frontaz;
  ingredients_it: Frontaz;
  ingredients_ka: Frontaz;
  ingredients_pl: Ingredientsde;
  ingredients_ro: Frontaz;
  ingredients_sq: Frontaz;
  nutrition: Ingredients;
  nutrition_az: Frontaz;
  nutrition_bg: Frontaz;
  nutrition_de: Frontaz;
  nutrition_en: Frontaz;
  nutrition_es: Frontaz;
  nutrition_fr: Frontaz;
  nutrition_ka: Frontaz;
  nutrition_pl: Nutritionpl;
  nutrition_ro: Frontaz;
  nutrition_sq: Frontaz;
  packaging_az: Frontaz;
  packaging_bg: Frontaz;
  packaging_en: Frontaz;
  packaging_fr: Frontar;
  packaging_ka: Frontaz;
  packaging_pl: Frontaz;
  packaging_ro: Frontaz;
  packaging_sq: Frontaz;
}

interface Nutritionpl {
  angle: string;
  geometry: string;
  imgid: string;
  normalize: string;
  ocr: number;
  orientation: string;
  rev: string;
  sizes: Sizes2;
  white_magic: string;
  x1: string;
  x2: string;
  y1: string;
  y2: string;
}

interface Ingredientsde {
  angle: string;
  geometry: string;
  imgid: string;
  normalize?: any;
  rev: string;
  sizes: Sizes2;
  white_magic?: any;
  x1: string;
  x2: string;
  y1: string;
  y2: string;
}

interface Ingredients {
  geometry: string;
  imgid: string;
  normalize?: any;
  rev: string;
  sizes: Sizes2;
  white_magic?: any;
}

interface Frontother {
  angle?: any;
  geometry: string;
  imgid: string;
  normalize?: any;
  rev: string;
  sizes: Sizes2;
  white_magic?: any;
  x1?: any;
  x2?: any;
  y1?: any;
  y2?: any;
}

interface Frontaz {
  angle: string;
  coordinates_image_size: string;
  geometry: string;
  imgid: string;
  normalize: string;
  rev: string;
  sizes: Sizes2;
  white_magic: string;
  x1: string;
  x2: string;
  y1: string;
  y2: string;
}

interface Frontar {
  angle: string;
  geometry: string;
  imgid: string;
  normalize: string;
  rev: string;
  sizes: Sizes2;
  white_magic: string;
  x1: string;
  x2: string;
  y1: string;
  y2: string;
}

interface Sizes2 {
  '100': _100;
  '200': _100;
  '400': _100;
  full: _100;
}

interface _35 {
  sizes: Sizes;
  uploaded_t: string;
  uploader: string;
}

interface _1 {
  sizes: Sizes;
  uploaded_t: number;
  uploader: string;
}

interface Sizes {
  '100': _100;
  '400': _100;
  full: _100;
}

interface _100 {
  h: number;
  w: number;
}

interface Ecoscoreextendeddata {
  impact: Impact;
}

interface Impact {
  ef_single_score_log_stddev: number;
  likeliest_impacts: Likeliestimpacts;
  likeliest_recipe: Likeliestrecipe;
  mass_ratio_uncharacterized: number;
  uncharacterized_ingredients: Uncharacterizedingredients;
  uncharacterized_ingredients_mass_proportion: Uncharacterizedingredientsmassproportion;
  uncharacterized_ingredients_ratio: Uncharacterizedingredientsmassproportion;
  warnings: string[];
}

interface Uncharacterizedingredientsmassproportion {
  impact: number;
  nutrition: number;
}

interface Uncharacterizedingredients {
  impact: string[];
  nutrition: any[];
}

interface Likeliestrecipe {
  'en:caffeine': number;
  'en:carbonated_water': number;
  'en:e150d': number;
  'en:e338': number;
  'en:sugar': number;
  'en:water': number;
}

interface Likeliestimpacts {
  Climate_change: number;
  EF_single_score: number;
}

interface Ecoscoredata {
  adjustments: Adjustments;
  ecoscore_not_applicable_for_category: string;
  status: string;
}

interface Adjustments {
}

interface Categoryproperties {
  'ciqual_food_name:en': string;
  'ciqual_food_name:fr': string;
}

interface Categoriesproperties {
  'agribalyse_food_code:en': string;
  'agribalyse_proxy_food_code:en': string;
  'ciqual_food_code:en': string;
}