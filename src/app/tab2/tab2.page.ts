import { Component } from '@angular/core';
import { BarcodeScanner } from '@awesome-cordova-plugins/barcode-scanner/ngx';
import { from } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { ProductRequest } from '../models/product';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  product: ProductRequest;

  constructor(
    private scanner : BarcodeScanner,
    private productService: ProductService,
  ) {}

  onClick() {
    // this.scanner.scan().then(result => {
    //   this.productService.getByCode(result.text).subscribe(data => {
    //     this.product = data;
    //   });
    // });

    from(this.scanner.scan()).pipe(
      mergeMap((result) => this.productService.getByCode(result.text))
    ).subscribe(data => this.product = data);
  }

}

