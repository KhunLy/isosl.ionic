import { Component } from '@angular/core';
import { ActionSheetController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  articles = localStorage.getItem('ARTICLES') ? JSON.parse(localStorage.getItem('ARTICLES')) : [
    { name: 'sel', isChecked: true },
    { name: 'poivre', isChecked: false },
  ];

  constructor(
    private actionSheetCtrl: ActionSheetController,
    private toastCtrl: ToastController
  ) {}

  async openAS(item: any) {
    let as = await this.actionSheetCtrl.create({
      header: 'Actions',
      buttons: [
        { text: 'Supprimer', icon: 'trash', role: 'destructive', 
          handler: async () => {
            this.articles = this.articles.filter(a => a !== item);
            let toast = await this.toastCtrl.create({ color: 'success', header: 'Supression OK', duration: 3000 })
            toast.present();
            localStorage.setItem('ARTICLES', JSON.stringify(this.articles));
          } },
        { text: 'Checker', icon: 'checkmark', 
          handler: async () => {
            item.isChecked = !item.isChecked;
            let toast = await this.toastCtrl.create({ color: 'success', header: 'Modif OK', duration: 3000 })
            toast.present();
            localStorage.setItem('ARTICLES', JSON.stringify(this.articles));
          }  },
      ]
    });
    as.present();
  }
}
