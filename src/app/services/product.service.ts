import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProductRequest } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
    private http: HttpClient
  ) { }

  public getByCode(code: string) {
    return this.http.get<ProductRequest>(
      `https://world.openfoodfacts.org/api/v0/product/${code}.json`);
  }
}
